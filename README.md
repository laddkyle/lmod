# iCER Flavored Lmod

### Documentation

iCER: [HPCC Wiki: Modules (Lmod)](https://wiki.hpcc.msu.edu/pages/viewpage.action?pageId=15434940&src=search)

TACC: [TACC Lmod](http://www.tacc.utexas.edu/tacc-projects/lmod)

### Compiling & Installing

```bash
# SDIR - directory to install Lmod (e.g. /opt/software)
# CACHEDIR - spider cache directory (e.g. /opt/software/lmod/cache)
# ANCIENT - how long is a spider cache valid? (in seconds)
./build-lmod.sh
```