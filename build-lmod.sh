#!/bin/bash
#
# Compiling Lmod
#

SDIR=$HOME/opt/software                 # path to install lmod: SDIR/lmod
CACHEDIR=/opt/software/lmod/cache       # location of system spider cache
ANCIENT=120960000                       # spider cache valid for 2 weeks

./configure --prefix=$SDIR                  \
            --with-ancient=$ANCIENT         \
            --with-spiderCacheDir=$CACHEDIR \
make
make install
